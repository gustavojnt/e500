import { useEffect, useState } from 'preact/hooks'

const fetchTweets = async () => {
  try {
    const url = new URL(process.env.API_FETCH_TWEET_URL)
    const res = await fetch(url)
    const { data } = await res.json()
    return data
  } catch (err) {
    console.error(err)
  }
}

export function useTweets() {
  const [tweets, setTweets] = useState([])

  useEffect(() => {
    // fetchTweets().then(setTweets)
    // TODO remove this
    fetchTweets().then((tweets) => {
      setTweets([...tweets, ...tweets, ...tweets])
    })
  }, [])

  return [tweets]
}
