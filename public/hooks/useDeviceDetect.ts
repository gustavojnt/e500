import { useEffect, useState } from 'preact/hooks'

export default function useDeviceDetect(): [boolean] {
  const notssr = typeof window !== 'undefined'

  const byUA = () => {
    if (notssr) {
      const userAgent =
        typeof window.navigator === 'undefined' ? '' : navigator.userAgent
      return Boolean(
        userAgent.match(
          /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
        )
      )
    }

    return true
  }

  const [isMobile, setMobile] = useState(() => byUA())

  const detectDevice = () => {
    setMobile(byUA())
  }

  useEffect(() => {
    detectDevice()

    return () => {
      notssr && window.removeEventListener('resize', detectDevice)
    }
  }, [])

  notssr && window.addEventListener('resize', detectDevice)

  return [isMobile]
}
