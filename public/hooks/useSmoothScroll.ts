import { StateUpdater, useState } from 'preact/hooks'

interface Options {
  duration: number
}

export function useSmoothScroll(
  options: Options
): [(time?: FrameRequestCallback) => void, StateUpdater<Options>] {
  const [opt, setOptions] = useState(options)

  function smoothScroll() {
    const scrollTotal =
      document.documentElement.scrollHeight - window.innerHeight
    const currentTop = document.documentElement.scrollTop
    const restScroll = scrollTotal - currentTop

    const factor = (1000 / opt.duration) * 10
    const to = currentTop + factor
    window.scroll({ top: to })

    if (restScroll) {
      window.requestAnimationFrame(smoothScroll)
    }
  }

  return [smoothScroll, setOptions]
}
