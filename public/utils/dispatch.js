const events = {
  tweet_click: ({ element, ...props }) => {
    return {
      ...props,
      element: `tweet-${element.username}-${element.id}`,
      elementCategory: 'post',
      pageSection: 'conteudo',
      pageSubsection: 'veja-o-que-estao-dizendo',
    }
  },
  tweet_scroll: ({ element, ...props }) => {
    return {
      ...props,
      element: `${element.index}-${element.length}`,
      elementCategory: 'feed',
      pageSection: 'conteudo',
      pageSubsection: 'veja-o-que-estao-dizendo',
      interactionType: 'scroll',
    }
  },
  field_fill: ({ element, ...props }) => {
    return {
      ...props,
      element: element,
      elementCategory: 'campo',
      interactionType: 'preencheu',
      pageSection: 'conteudo',
      pageSubsection: 'lead-cadastro',
    }
  },
  field_select: ({ element, ...props }) => {
    return {
      ...props,
      element: element,
      elementCategory: 'campo',
      interactionType: 'selecao',
      pageSection: 'conteudo',
      pageSubsection: 'lead-cadastro',
    }
  },
  form_submit: ({ element, ...props }) => {
    return {
      ...props,
      element: element,
      elementCategory: 'submit',
      interactionType: 'clique',
      pageSection: 'conteudo',
      pageSubsection: 'lead-cadastro',
    }
  },
}

export function dispatch(event, details) {
  if (events.hasOwnProperty(event)) {
    const newEvent = events[event]({ ...details })
    window.dataLayer.push({ ...newEvent })
    log({ eventName: event, ...newEvent })
  } else {
    console.error('event not implemented...')
  }
}

function log({ eventName, ...rest }) {
  console.info(
    '%c%s',
    `
    padding: 5px 10px;
    border-radius: 4px;
    border: 1px solid rgba(211,210,203,1);
    background-color: rgba(38,143,81,0.8);
    font-weight: bold;
    color: #ffffff;
    `,
    eventName,
    rest
  )
}
