export function getCookie(name: string) {
  if (name === 'google_client_id') {
    return (document.cookie.match(/\b_ga=([^;]*)/) || [])[1] || ''
  }

  if (name === 'clientId') {
    return (document.cookie.match(/\bIsobar_ClientID=([^;]*)/) || [])[1] || null
  }

  const cookie = document.cookie.match(`(^|;) ?${name}=([^;]*)(;|$)`)
  return cookie && cookie.length >= 2 ? decodeURIComponent(cookie[2]) : ''
}

export function getCookieDirect(name: string) {
  const cookie = getCookie(name)
  return cookie.match(/\(?direct|direto\)?/gi) ? 'DIRECT' : cookie
}
