export function getDeviceName() {
  let device

  if (navigator.userAgent.match(/Tablet|iPad/i)) {
    device = 'Tablet'
  } else if (
    navigator.userAgent.match(
      /IEMobile|Windows Phone|Lumia|Android|webOS|iPhone|iPod|Blackberry|PlayBook|BB10|Mobile Safari|Opera Mini|\bCrMo\/|Opera Mobi/i
    )
  ) {
    device = 'Mobile'
  } else {
    device = 'Desktop'
  }

  return device
}
