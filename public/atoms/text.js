import { WithComponent } from './withComponent'

function TextAtom({ className = '', ...props }) {
  return (
    <div className={className} {...props}>
      {props.children}
    </div>
  )
}

TextAtom.displayName = 'Text^Atom'

export const Text = WithComponent(TextAtom)
