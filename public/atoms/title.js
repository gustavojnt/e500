import { WithComponent } from './withComponent'

function TitleAtom({ text, className = '', ...props }) {
  return (
    <h1 className={[className].join(' ').trim()} {...props}>
      {text}
    </h1>
  )
}

TitleAtom.displayName = 'Title^atom'

export const Title = WithComponent(TitleAtom)
