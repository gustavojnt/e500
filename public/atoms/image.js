import { WithComponent } from './withComponent'

function ImageAtom({ pictureClasses = '', src = '', sizes = [], ...props }) {
  return (
    <picture className={pictureClasses}>
      {sizes.map(([media, srcset] = size) => (
        <source media={media} srcset={srcset} />
      ))}
      <img src={src} alt="" {...props} />
    </picture>
  )
}

ImageAtom.displayName = 'Image^Atom'

export const Image = WithComponent(ImageAtom)
