import { useState } from 'preact/hooks'
import { WithComponent } from './withComponent'

function FormAtom({ onSubmit, children, ...props }) {
  return (
    <form
      method="POST"
      onSubmit={(evt) => {
        evt.preventDefault()
        onSubmit({ formData: new FormData(evt.target) })
      }}
      {...props}
    >
      {children}
    </form>
  )
}

FormAtom.displayName = 'Form^atom'

export const Form = WithComponent(FormAtom)
