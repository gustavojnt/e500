import { WithComponent } from './withComponent'

function HeaderAtom({ text, className = '', ...props }) {
  return (
    <header className={['flex-center', ' ', className].join``} {...props}>
      {text}
    </header>
  )
}

HeaderAtom.displayName = 'Header^atom'

export const Header = WithComponent(HeaderAtom)
