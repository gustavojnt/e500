import { WithComponent } from './withComponent'

function GridAtom({
  className = '',
  cols = 1,
  rows = 1,
  span,
  start,
  end,
  customCols = [],
  style: styleGrid = {},
  ...props
}) {
  const haveSpanGrid = span || start || end
  const isColsArray = Array.isArray(cols)
  const isRowsArray = Array.isArray(rows)
  const styleColumns = isColsArray && {
    gridTemplateColumns: cols.join(' '),
  }
  const styleRows = isRowsArray && {
    gridTemplateRows: rows.join(' '),
  }

  return (
    <div
      className={[`grid`]
        .concat(!isRowsArray && `grid-rows-${rows}`)
        .concat(!isColsArray && `grid-cols-${cols}`)
        .concat(className)
        .filter(Boolean)
        .join(' ')
        .trim()}
      style={{ ...styleColumns, ...styleRows, ...styleGrid }}
      {...props}
    >
      {haveSpanGrid ? (
        <div
          className={['z-10']
            .concat(span && `col-span-${span}`)
            .concat(start && `col-start-${start}`)
            .concat(end && `col-end-${end}`)
            .filter(Boolean)
            .join(' ')}
        >
          {props.children}
        </div>
      ) : (
        props.children
      )}
    </div>
  )
}

GridAtom.displayName = 'Grid^atom'

export const Grid = WithComponent(GridAtom)
