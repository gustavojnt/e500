import { WithComponent } from './withComponent'

/**
 *
 * @param {['row-start-{n}/col-start-{n}', 'row-end-{n}/col-end-{n}']} grid
 * @returns
 */
function AreaAtom({ grid = ['1/1', '2/2'], style = {}, ...props }) {
  return (
    <div style={{ gridArea: grid.join(' / '), ...style }} {...props}>
      {props.children}
    </div>
  )
}

AreaAtom.displayName = 'Area^Atom'

export const Area = WithComponent(AreaAtom)
