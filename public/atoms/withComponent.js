export const WithComponent = Component => props => { 
  return (
    <Component data-component={Component.displayName} {...props} />
  );
};
