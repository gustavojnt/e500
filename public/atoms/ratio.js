import { WithComponent } from './withComponent'

function RatioAtom({ aspect, ...props }) {
  const [w, h] = aspect.split(':')

  return (
    <div className={`aspect-w-${w} aspect-h-${h}`} {...props}>
      {props.children}
    </div>
  )
}

RatioAtom.displayName = 'Ratio^Atom'

export const Ratio = WithComponent(RatioAtom)
