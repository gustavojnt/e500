import { WithComponent } from "./withComponent"

function LabelAtom({ text, ...props }) {
	return <label {...props}>{text}</label>
}

LabelAtom.displayName = "Label^atom"

export const Label = WithComponent(LabelAtom)