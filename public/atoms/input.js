import { WithComponent } from './withComponent'

const TYPES = {
  TEXT: 'text',
  EMAIL: 'email',
  TEXTAREA: 'textarea',
  NUMBER: 'number',
  SELECT: 'select',
  HIDDEN: 'hidden',
}

function InputAtom({ name, type = 'text', ...props }) {
  switch (type) {
    case TYPES.TEXT:
      return (
        <input
          type="text"
          autocomplete="autocomplete_off_hack_xfr4!k"
          name={name}
          {...props}
        />
      )
    case TYPES.EMAIL:
      return (
        <input
          type="email"
          autocomplete="autocomplete_off_hack_xfr4!k"
          name={name}
          {...props}
        />
      )
    case TYPES.TEXTAREA:
      return (
        <textarea
          name={name}
          rows={2}
          style={{ height: 'auto' }}
          {...props}
        ></textarea>
      )
    case TYPES.NUMBER:
      return (
        <input
          type="tel"
          autocomplete="autocomplete_off_hack_xfr4!k"
          name={name}
          {...props}
        />
      )
    case TYPES.SELECT:
      return (
        <select name={name} {...props}>
          {props.children}
        </select>
      )
    case TYPES.HIDDEN:
      return <input type="hidden" name={name} {...props} />

    default:
      throw new Error(`Invalid input type: ${type}`)
  }
}

InputAtom.displayName = 'Input^atom'

export const Input = WithComponent(InputAtom)
