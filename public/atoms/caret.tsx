import { WithComponent } from "./withComponent";

interface Props {
  direction?: "top" | "right" | "bottom" | "left";
  color?: string;
  className?: string;
  [props: string]: string;
}

function CaretAtom({ direction = "top", color = "var(--white)", className = "", ...props }: Props) {
  const rotate = {
    right: "rotate-90",
    left: "-rotate-90",
    bottom: "rotate-180",
  }

  return (
    <svg
      className={["transform", "w-4", "h-4", "cursor-pointer"].concat(rotate[direction]).concat(className).join(" ")}
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 26 15"
      {...props}
    >
      <polygon fill={color} points="23.303,-0.002 12.467,10.834 1.63,-0.002 -0.454,2.082 12.467,15.002 14.551,12.918 25.387,2.082" />
    </svg>
  );
}

CaretAtom.displayName = 'Caret^atom'

export const Caret = WithComponent(CaretAtom);
