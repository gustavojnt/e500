import { WithComponent } from './withComponent'

function BarAtom({ className = '', ...props }) {
  return (
    <svg
      className={className}
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      width="98.786"
      height="51.417"
      viewBox="0 0 98.786 51.417"
      {...props}
    >
      <defs>
        <linearGradient
          id="a"
          x1="0.5"
          x2="0.5"
          y2="1"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stop-color="#00e2ff" />
          <stop offset="1" stop-color="#ba00ff" />
        </linearGradient>
        <filter
          id="b"
          x="0"
          y="0"
          width="98.786"
          height="51.417"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="3" result="c" />
          <feFlood flood-color="#d800ff" flood-opacity="0.902" />
          <feComposite operator="in" in2="c" />
          <feComposite in="SourceGraphic" />
        </filter>
        <filter
          id="d"
          x="0"
          y="0"
          width="98.786"
          height="51.417"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="3" input="SourceAlpha" />
          <feGaussianBlur stdDeviation="3" result="e" />
          <feFlood flood-color="#ce00ff" flood-opacity="0.686" result="f" />
          <feComposite operator="out" in="SourceGraphic" in2="e" />
          <feComposite operator="in" in="f" />
          <feComposite operator="in" in2="SourceGraphic" />
        </filter>
      </defs>
      <g data-type="innerShadowGroup">
        <g class="d" transform="matrix(1, 0, 0, 1, 0, 0)" fill="url(#b)">
          <rect
            class="a"
            fill="url(#a)"
            opacity="0.794"
            width="33.417"
            height="80.786"
            transform="translate(89.79 6) rotate(90)"
          />
        </g>
        <g class="c" transform="matrix(1, 0, 0, 1, 0, 0)" fill="url(#d)">
          <rect
            class="b"
            fill="url(#a)"
            opacity="0.794"
            width="33.417"
            height="80.786"
            transform="translate(89.79 6) rotate(90)"
          />
        </g>
      </g>
    </svg>
  )
}

BarAtom.displayName = 'Bar^atom'

export const Bar = WithComponent(BarAtom)
