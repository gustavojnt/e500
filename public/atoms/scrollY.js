import { useEffect, useState } from 'preact/hooks'
import { track } from './dataLayer'
import { WithComponent } from './withComponent'

function ScrollYAtom({ children, height, className = '', ...props }) {
  const [io, setIo] = useState(null)
  const [length, setLength] = useState(0)
  let lastIndex

  const handleRef = (ref) => {
    ref && setLength(ref.childElementCount)
  }

  let debouncing
  const handleObserver = (entities) => {
    const [entity] = entities

    if (entity.isIntersecting) {
      clearTimeout(debouncing)
      debouncing = setTimeout(() => {
        const { target } = entity
        lastIndex && track('tweet_scroll', { index: lastIndex, length })
        lastIndex = target.tabIndex + 1
      }, 350)
    }
  }

  useEffect(() => {
    const options = {
      root: null,
      rootMargin: '0px',
      threshold: 1.0,
    }

    length && setIo(new IntersectionObserver(handleObserver, options))

    return () => {
      io?.disconnect()
    }
  }, [length])

  const observe = (ref) => {
    if (ref) {
      io?.observe(ref.base)
    }
  }

  return (
    <div
      ref={handleRef}
      className={`overflow-y-auto overflow-x-hidden overflow-dcode ${className}`}
      style={{ height }}
      {...props}
    >
      {children({ observe })}
    </div>
  )
}

ScrollYAtom.displayName = 'ScrollY^atom'

export const ScrollY = WithComponent(ScrollYAtom)
