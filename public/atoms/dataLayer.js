import { dispatch } from '../utils/dispatch'

const event = {
  event: 'interaction',
  brand: 'fiat',
  segment: 'landing-page',
  product: '500e',
  path: 'solta-raio',
  selectedValue: null,
}

export const track = (eventName = '', element = {}) => {
  dispatch(eventName, {
    ...event,
    ...{
      element,
    },
  })
}

export function DataLayer({ children, element = {}, event = '' }) {
  const handleTrack = () => {
    track(event, element)
  }

  return children({
    track: handleTrack,
  })
}
