import { WithComponent } from './withComponent'

function ButtonAtom({ text, ...props }) {
  return <button {...props}>{text}</button>
}

ButtonAtom.displayName = 'Button^atom'

export const Button = WithComponent(ButtonAtom)
