import {
  LocationProvider,
  Router,
  Route,
  ErrorBoundary,
  hydrate,
  prerender as ssr,
} from 'preact-iso'
import { Home } from './pages/home'
import NotFound from './pages/_404.js'
import { Header } from './molecules/header'
import { useScript } from 'hoofd'

export function App() {
  return (
    <LocationProvider>
      <Header />
      <ErrorBoundary>
        <Router>
          <Route path="/" component={Home} />
          <Route default component={NotFound} />
        </Router>
      </ErrorBoundary>
    </LocationProvider>
  )
}

hydrate(<App />)

export async function prerender(data) {
  return await ssr(<App {...data} />)
}
