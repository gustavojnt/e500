import { WithComponent } from '../atoms/withComponent'
import { Area } from '../atoms/area'
import { Text } from '../atoms/text'
import { Grid } from '../atoms/grid'
import { Image } from '../atoms/image'

function BateryMolecule({ value = 0, className = '', ...props }) {
  return (
    <Grid className={['items-center', ' ', className].join``} {...props}>
      <Area
        className="justify-self-center absolute -left-16"
        grid={['1/1', '2/2']}
        style={{ top: '29%' }}
      >
        <Text className="text-white font-HeroLight font-bold">60%</Text>
      </Area>
      <Area
        className="w-full justify-self-center absolute"
        grid={['1/1', '2/2']}
        style={{ top: '41%' }}
      >
        <hr
          className="-ml-6 border-dashed"
          style={{ width: 'calc(100% + 3rem)' }}
        />
      </Area>
      <Area className="justify-self-center w-full" grid={['1/1', '2/2']}>
        <Image
          className="w-full"
          src="/assets/stack-85w.svg"
          sizes={[
            ['(min-width: 1920px)', '/assets/stack-85w.svg'],
            ['(min-width: 1024px)', '/assets/stack-85w.svg'],
            ['(min-width: 512px)', '/assets/stack-85w.svg'],
            ['(max-width: 511px)', '/assets/stack-85w.svg'],
          ]}
        />
      </Area>
      <Area
        className="w-full justify-self-center flex-center z-10"
        grid={['1/1', '2/2']}
      >
        <Image
          className="w-full"
          src="/assets/lightning-21w.svg"
          sizes={[
            ['(min-width: 512px)', '/assets/lightning-21w.svg'],
            ['(max-width: 511px)', '/assets/lightning-21w.svg'],
          ]}
        />
      </Area>
      <Area
        className="w-full justify-self-center self-end"
        grid={['1/1', '2/2']}
      >
        <Image
          className="w-full"
          src="/assets/charge-85w.svg"
          sizes={[
            ['(min-width: 1920px)', '/assets/charge-85w.svg'],
            ['(min-width: 1024px)', '/assets/charge-85w.svg'],
            ['(min-width: 512px)', '/assets/charge-85w.svg'],
            ['(max-width: 511px)', '/assets/charge-85w.svg'],
          ]}
        />
      </Area>
    </Grid>
  )
}

BateryMolecule.displayName = 'Batery^molecule'

export const Batery = WithComponent(BateryMolecule)
