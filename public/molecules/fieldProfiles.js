import { track } from '../atoms/dataLayer'
import { WithComponent } from '../atoms/withComponent'
import { InputWrapper } from './withField'

function FieldProfilesMolecule({ label, name, ...props }) {
  const profiles = [
    { value: 'Vendas', name: 'Pessoa Física - CPF' },
    {
      value: 'Vendas Diretas - Autonomy',
      name: 'Pessoa com Deficiência - PcD',
    },
    {
      value: 'Vendas Diretas - PJ - Micro empresário',
      name: 'Micro empresário - CNPJ',
    },
    { value: 'Vendas Diretas - Taxista', name: 'Taxista' },
    {
      value: 'Vendas Diretas - Produtor Rural',
      name: 'Produtor Rural',
    },
    { value: 'Vendas Diretas - PJ - Locadora', name: 'Locadora' },
    {
      value: 'Vendas Diretas - PJ - Auto Escola',
      name: 'Auto Escola',
    },
  ]

  return (
    <InputWrapper
      validators={['required']}
      onSelect={() => track('field_select', label)}
      {...props}
    >
      {({ Label, Input }) => (
        <>
          <Label for={name} text={label} />
          <Input id={name} type="select" name={name}>
            {profiles.map(({ value, name }) => (
              <option style={{ backgroundColor: 'var(--black)' }} value={value}>
                {name}
              </option>
            ))}
          </Input>
        </>
      )}
    </InputWrapper>
  )
}

FieldProfilesMolecule.displayName = 'FieldProfiles^molecule'

export const FieldProfiles = WithComponent(FieldProfilesMolecule)
