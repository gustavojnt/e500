import { Label } from './../atoms/label'
import { Input } from './../atoms/input'
import { useEffect, useRef } from 'preact/hooks'

export function InputWrapper({
  children,
  className = '',
  validators = [],
  onChange = () => {},
  onBlur = () => {},
  onFill = () => {},
  onSelect = () => {},
  ...props
}) {
  const errorEl = useRef()
  const inputEl = useRef()
  let debouncing

  useEffect(() => {
    if (validators.includes('required')) {
      inputEl.current.base.required = true
    }
  }, [])

  const handleInputBlur = ({ target: { value } }) => {
    errorEl.current.textContent = ''
    if (validators.includes('required')) {
      if (value === '') {
        errorEl.current.textContent = 'Campo requerido...'
      }
    }
    onBlur({ value })
  }

  const renderLabel = ({ className = '', ...props }) => (
    <Label
      className={['text-white mb-1 font-FuturaPTBook']
        .concat(className)
        .join(' ')
        .trim()}
      {...props}
    />
  )

  const renderInput = ({ className = '', ...props }) => (
    <Input
      ref={inputEl}
      className={[
        'h-9 outline-none bg-transparent border p-2 text-white font-FuturaPTBook',
      ]
        .concat(className)
        .join(' ')
        .trim()}
      onBlur={handleInputBlur}
      onKeyup={({ target: { value } }) => {
        onChange({ value })
        clearTimeout(debouncing)
        debouncing = setTimeout(() => {
          onFill()
        }, 1000)
      }}
      onChange={({ target: { value } }) => {
        onSelect({ value })
      }}
      {...props}
    />
  )

  const renderError = ({ className = '', ...props }) => (
    <span
      ref={errorEl}
      className="text-red-400 mt-1 uppercase text-xs"
      {...props}
    ></span>
  )

  return (
    <div className={['flex flex-col mb-4', className].join(' ')} {...props}>
      {children({
        Label: renderLabel,
        Input: renderInput,
        Error: renderError,
      })}
      {renderError({})}
    </div>
  )
}

export const WithField = (Field) => (props) => {
  return <Field data-component={Field.displayName} {...props} />
}
