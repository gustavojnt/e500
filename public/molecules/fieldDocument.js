import { useRef } from 'preact/hooks'
import { track } from '../atoms/dataLayer'
import { WithComponent } from '../atoms/withComponent'
import { isCNPJ, maskCNPJ } from '../utils/isCNPJ'
import { isCPF, maskCPF } from '../utils/isCPF'
import { InputWrapper } from './withField'

function FieldDocumentMolecule({ label, name, ...props }) {
  const inputEl = useRef()
  const errorEl = useRef()

  const handleInputChange = ({ value }) => {
    if (isCPF(value)) {
      inputEl.current.base.value = maskCPF(value)
    }

    if (isCNPJ(value)) {
      inputEl.current.base.value = maskCNPJ(value)
    }
  }

  const handleInputBlur = ({ value }) => {
    errorEl.current.base.textContent = ''
    if (isCPF(value) || isCNPJ(value)) {
      errorEl.current.base.textContent = ''
    } else {
      if (value == '') return
      errorEl.current.base.textContent = 'Campo inválido... Tente novamente.'
    }
  }

  return (
    <InputWrapper
      validators={['required']}
      onChange={handleInputChange}
      onBlur={handleInputBlur}
      onFill={() => track('field_fill', label)}
      {...props}
    >
      {({ Label, Input, Error }) => (
        <>
          <Label for={name} text={label} />
          <Input id={name} ref={inputEl} type="number" name={name} />
          <Error ref={errorEl} />
        </>
      )}
    </InputWrapper>
  )
}

FieldDocumentMolecule.displayName = 'FieldDocument^molecule'

export const FieldDocument = WithComponent(FieldDocumentMolecule)
