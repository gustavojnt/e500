import { WithComponent } from '../atoms/withComponent'
import { Button } from '../atoms/button'
import { Form } from '../atoms/form'
import { FieldName } from '../molecules/fieldName'
import { FieldEmail } from '../molecules/fieldEmail'
import { FieldTel } from '../molecules/fieldTel'
import { FieldProfiles } from '../molecules/fieldProfiles'
import { FieldDocument } from '../molecules/fieldDocument'
import { FieldMessage } from '../molecules/fieldMessage'
import { isCPF } from '../utils/isCPF'
import { isCNPJ } from '../utils/isCNPJ'
import useDeviceDetect from '../hooks/useDeviceDetect'
import { getDeviceName } from '../utils/getDevice'
import { getCookie, getCookieDirect } from '../utils/getCookie'
import { track } from '../atoms/dataLayer'

function FormLeadMolecule({ children, onSubmit, ...props }) {
  const [isMobile] = useDeviceDetect()

  const handleFormSubmit = ({ formData }) => {
    const doc = formData.get('document')

    let values = {
      firstName: formData.get('name'),
      email: formData.get('email'),
      phone: [formData.get('phoneDigit'), formData.get('phoneNumber')].join(
        ' '
      ),
      cpf: formData.get('document'),
      origin: 'Internet',
      SubOrigem__c: 'Brand Web Site',
      retUrl: 'www.fiat.com.br',
      form: 'Proposal',
      productGroup: 'Vendas',
      source: getCookieDirect('utm_source'),
      medium: getCookie('utm_medium'),
      campaign: getCookie('utm_campaign'),
      content: getCookie('utm_content'),
      term: getCookie('utm_term'),
      clientId: getCookie('clientId'),
      google_client_id: getCookie('google_client_id'),
      device: getDeviceName(),
      ga_client_id: getCookie('_ga'),
      ga_transaction_id: new Date().getTime() + formData.get('document'),
      brand: 'fiat',
      testDrive: null,
      referer: window.location.href,
      isWhatsappBotIntegrated: false,
      indEmailContact: true,
      indSmsContact: true,
      indWhatsappContact: true,
      isWhatsappNumber: false,
      indPhoneContact: true,
    }

    if (isCPF(doc)) {
      values = { ...values, ...{ cpf: doc } }
    }

    if (isCNPJ(doc)) {
      values = { ...values, ...{ cnpj: doc } }
    }

    onSubmit({ values })
  }

  return (
    <Form
      className="lg:grid lg:gap-y-0 lg:gap-x-8"
      onSubmit={handleFormSubmit}
      {...props}
    >
      <FieldName
        className="lg:col-start-1"
        label="Nome completo"
        name="name"
        {...props}
      />
      <FieldEmail
        className="lg:col-start-1"
        label="E-mail"
        name="email"
        {...props}
      />
      <FieldTel
        className="lg:col-start-1"
        label="Telefone"
        name="phone"
        {...props}
      />
      <FieldProfiles
        className="lg:col-start-2 lg:row-start-1"
        label="Selecione o seu perfil"
        name="profile"
        {...props}
      />
      <FieldDocument
        className="lg:col-start-2 lg:row-start-2"
        label="CPF/CNPJ"
        name="document"
        {...props}
      />
      <FieldMessage
        className="lg:col-start-2 lg:row-start-3"
        label="Mensagem"
        name="message"
        {...props}
      />
      <Button
        className="mt-4 mb-8 w-full lg:w-1/2 lg:justify-self-center py-2 text-white text-2xl rounded font-FuturaPTMedium"
        style={{
          background: 'var(--cyan1)',
          gridArea: !isMobile && '5/1 / 5/3',
        }}
        onClick={() => track('form_submit', 'Enviar')}
        {...props}
      >
        Enviar
      </Button>
    </Form>
  )
}

FormLeadMolecule.displayName = 'FormLead^molecule'

export const FormLead = WithComponent(FormLeadMolecule)
