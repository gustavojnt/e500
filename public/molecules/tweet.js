import { Image } from '../atoms/image'
import { Text } from '../atoms/text'
import { Grid } from '../atoms/grid'
import { WithComponent } from '../atoms/withComponent'
import { Area } from '../atoms/area'
import { Ratio } from '../atoms/ratio'
import { DataLayer } from '../atoms/dataLayer'

function TweetMolecule({
  bgColor = '',
  tweet,
  className = '',
  children,
  ...props
}) {
  const renderTweetAvatar = ({ src, className = '', ...props }) => (
    <Area grid={['1/1', '1/1']}>
      <Image className={`rounded-full ${className}`} src={src} {...props} />
    </Area>
  )

  const renderTweetUser = ({ className = '', children, ...props }) => (
    <Area className="self-center" grid={['1/2', '1/2']}>
      <Text className={`ml-3 ${className}`} {...props}>
        {children}
      </Text>
    </Area>
  )

  const renderTweetPost = ({ className = '', children, ...props }) => (
    <Area
      className="mt-2 self-center overflow-hidden overflow-ellipsis"
      grid={['2/1', '2/3']}
      style={{
        maxHeight: '11.5vw',
        display: '-webkit-box',
        '-webkit-line-clamp': 2,
        '-webkit-box-orient': 'vertical',
      }}
    >
      <Text className={`${className}`} {...props}>
        {children}
      </Text>
    </Area>
  )

  return (
    <DataLayer element={tweet} event="tweet_click">
      {({ track }) => (
        <div
          className={`${className}`}
          style={{ background: bgColor || 'auto' }}
          onClick={track}
          tabIndex={tweet.index}
          {...props}
        >
          <Ratio aspect="283:107">
            <Grid
              cols={['40px', '1fr']}
              rows={['auto', '1fr']}
              className="rounded py-3 px-4"
            >
              {children({
                TweetAvatar: renderTweetAvatar,
                TweetUser: renderTweetUser,
                TweetPost: renderTweetPost,
              })}
            </Grid>
          </Ratio>
        </div>
      )}
    </DataLayer>
  )
}

TweetMolecule.displayName = 'Tweet^molecule'

export const Tweet = WithComponent(TweetMolecule)
