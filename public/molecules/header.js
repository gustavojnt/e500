import { Flag } from '../atoms/flag'
import { Title } from '../atoms/title'
import { WithComponent } from '../atoms/withComponent'

function HeaderMolecule({ ...props }) {
  return (
    <header
      style={{ background: 'var(--purple5)' }}
      className="py-3.5 lg:py-2 flex-center z-50 sticky top-0"
      {...props}
    >
      <Flag className="w-8 mr-2" />
      <Title
        className="text-lg lg:text-sm text-white font-FuturaPTBold"
        text="FIAT 500e"
      />
    </header>
  )
}

HeaderMolecule.displayName = 'Header^molecule'

export const Header = WithComponent(HeaderMolecule)
