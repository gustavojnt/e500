import { track } from '../atoms/dataLayer'
import { WithComponent } from '../atoms/withComponent'
import { InputWrapper } from './withField'

function FieldEmailMolecule({ label, name, ...props }) {
  return (
    <InputWrapper
      validators={['required']}
      {...props}
      onFill={() => track('field_fill', label)}
    >
      {({ Label, Input }) => (
        <>
          <Label for={name} text={label} />
          <Input id={name} type="email" name={name} />
        </>
      )}
    </InputWrapper>
  )
}

FieldEmailMolecule.displayName = 'FieldEmail^molecule'

export const FieldEmail = WithComponent(FieldEmailMolecule)
