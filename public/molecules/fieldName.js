import { track } from '../atoms/dataLayer'
import { WithComponent } from '../atoms/withComponent'
import { InputWrapper } from './withField'

function FieldNameMolecule({ label, name, ...props }) {
  return (
    <InputWrapper
      validators={['required']}
      {...props}
      onFill={() => track('field_fill', label)}
    >
      {({ Label, Input }) => (
        <>
          <Label for={name} text={label} />
          <Input id={name} name={name} />
        </>
      )}
    </InputWrapper>
  )
}

FieldNameMolecule.displayName = 'FieldName^molecule'

export const FieldName = WithComponent(FieldNameMolecule)
