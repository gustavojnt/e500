import { track } from '../atoms/dataLayer'
import { WithComponent } from '../atoms/withComponent'
import { InputWrapper } from './withField'

function FieldTelMolecule({ label, ...props }) {
  return (
    <InputWrapper
      validators={['required']}
      onFill={() => track('field_fill', label)}
      {...props}
    >
      {({ Label, Input }) => (
        <>
          <Label for="phoneDigit" text={label} />
          <div className="flex space-x-2.5">
            <Input
              id="phoneDigit"
              className="w-1/5"
              type="number"
              name="phoneDigit"
              maxlength="2"
            />
            <Input
              id="phoneNumber"
              className="w-4/5"
              type="number"
              name="phoneNumber"
              maxlength="10"
            />
          </div>
        </>
      )}
    </InputWrapper>
  )
}

FieldTelMolecule.displayName = 'FieldTel^molecule'

export const FieldTel = WithComponent(FieldTelMolecule)
