import { track } from '../atoms/dataLayer'
import { WithComponent } from '../atoms/withComponent'
import { InputWrapper } from './withField'

function FieldMessageMolecule({ label, name, ...props }) {
  return (
    <InputWrapper
      validators={['required']}
      {...props}
      onFill={() => track('field_fill', label)}
    >
      {({ Label, Input }) => (
        <>
          <Label for={name} text={label} />
          <Input id={name} type="textarea" rows={3} name={name} />
        </>
      )}
    </InputWrapper>
  )
}

FieldMessageMolecule.displayName = 'FieldMessage^molecule'

export const FieldMessage = WithComponent(FieldMessageMolecule)
