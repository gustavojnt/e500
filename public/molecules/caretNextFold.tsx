import { Caret } from "../atoms/caret";
import { WithComponent } from "../atoms/withComponent";
import { useSmoothScroll } from "../hooks/useSmoothScroll";

interface Props {
  goto: string;
  className?: string;
  [props: string]: string;
}

function CaretNextFoldMolecule({ goto, className = "", ...props }: Props) {
  const [smoothScroll] = useSmoothScroll({ duration: 1000 })

  const handleCaretClick = () => {
    console.log('detected')
    smoothScroll()
  }

  return (
    <Caret
      className="w-7 h-7 m-auto mt-10 animate-bounce lg:z-50"
      onClick={handleCaretClick}
      direction="bottom"
      {...props}
    />
  );
}

CaretNextFoldMolecule.displayName = 'CaretNextFold^molecule'

export const CaretNextFold = WithComponent(CaretNextFoldMolecule);

