import { Title } from '../atoms/title'
import { Header } from '../atoms/header'
import { WithComponent } from '../atoms/withComponent'
import { FormLead } from '../molecules/formLead'
import { useRef } from 'preact/hooks'

function FormFoldOrganism({ ...props }) {
  const formRef = useRef()

  const handleFormSubmit = async ({ values }) => {
    try {
      await fetch('https://lead.fcalatam.com.br/HubMicroservices', {
        headers: {
          'Content-Type': 'application/json',
        },
        method: 'post',
        body: JSON.stringify(values),
      })

      const formEl = formRef?.current?.base
      const buttonEl = formEl.querySelector('button')
      buttonEl.textContent = `Enviado!`
      formEl.reset()
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div {...props}>
      <Header className="bg-gradient-to-t from-black to-transparent">
        <Title className="text-center text-lg leading-tight my-7 max-w-25ch text-white font-FuturaPTBold">
          Cadastre-se para receber <br /> conteudo em primeira mão.
        </Title>
      </Header>
      <FormLead ref={formRef} onSubmit={handleFormSubmit} />
    </div>
  )
}

FormFoldOrganism.displayName = 'FormFold^organism'

export const FormFold = WithComponent(FormFoldOrganism)
