import { WithComponent } from '../atoms/withComponent'
import { ScrollY } from '../atoms/scrollY'
import { Title } from '../atoms/title'
import { Image } from '../atoms/image'
import { Tweet } from '../molecules/tweet'
import { CaretNextFold } from '../molecules/caretNextFold'
import { useTweets } from '../hooks/useTweets'
import useDeviceDetect from '../hooks/useDeviceDetect'

function TweetFoldOrganism({ className = '', ...props }) {
  const [isMobile] = useDeviceDetect()
  const [tweets] = useTweets()

  return (
    <>
      {isMobile && (
        <Image
          class="w-full h-full absolute inset-0 z-10"
          src="/assets/subhero-750w.png"
          sizes={[
            ['(min-width: 512px)', '/assets/subhero-750w.png'],
            ['(max-width: 511px)', '/assets/subhero-375w.png'],
          ]}
        />
      )}
      <div className="bg-black" />
      <div
        className={['pt-20 pr-6 lg:pr-0 lg:pt-32 z-20 lg:z-30', className].join(
          ' '
        )}
        {...props}
      >
        <Title className="text-white text-center text-md mb-4 pt-2 font-HeroLight leading-tight">
          Veja o que estão <br /> dizendo sobre <strong>#SOLTARAIO</strong>
        </Title>
        <ScrollY
          className="pl-8 lg:pl-4 pr-4 lg:pr-8 space-y-4"
          height={isMobile ? '80vw' : '65vh'}
        >
          {({ observe }) =>
            tweets.map((tweet, index) => (
              <Tweet
                ref={observe}
                className="text-white backdrop-filter backdrop-blur-3xl lg:opacity-70"
                bgColor={!isMobile && 'var(--black)'}
                tweet={{ ...tweet, index }}
              >
                {({ TweetAvatar, TweetUser, TweetPost }) => (
                  <>
                    <TweetAvatar src={tweet.avatar} />
                    <TweetUser className="text-sm font-HeroLight">
                      {tweet.username}
                    </TweetUser>
                    <TweetPost className="text-sm font-HeroLight">
                      {tweet.post}
                    </TweetPost>
                  </>
                )}
              </Tweet>
            ))
          }
        </ScrollY>
        <CaretNextFold />
      </div>
    </>
  )
}

TweetFoldOrganism.displayName = 'TweetFold^organism'

export const TweetFold = WithComponent(TweetFoldOrganism)
