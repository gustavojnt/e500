import { WithComponent } from '../atoms/withComponent'
import { Image } from '../atoms/image'
import { Text } from '../atoms/text'
import { Batery } from '../molecules/batery'
import { Title } from '../atoms/title'
import useDeviceDetect from '../hooks/useDeviceDetect'

function FirstFoldOrganism() {
  const [isMobile] = useDeviceDetect()

  return (
    <>
      <Image
        class="w-full h-full absolute inset-0 bg-black"
        src="/assets/hero-750w.png"
        sizes={[
          ['(min-width: 1920px)', '/assets/hero-2732w.png'],
          ['(min-width: 1024px)', '/assets/hero-1366w.png'],
          ['(min-width: 512px)', '/assets/hero-750w.png'],
          ['(max-width: 511px)', '/assets/hero-375w.png'],
        ]}
      />
      <div className="absolute inset-0 w-full h-full flex flex-col items-center lg:justify-start z-30">
        <Image
          className="mt-20 w-9/12 lg:w-3/5"
          pictureClasses="flex justify-center"
          src="/assets/logo-392w.png"
          sizes={[
            ['(min-width: 512px)', '/assets/logo-392w.png'],
            ['(max-width: 511px)', '/assets/logo-196w.png'],
          ]}
        />
        <Title className="text-white font-HeroLight text-center mt-1">
          <Text className="text-xs lg:text-lg leading-normal tracking-wider font-bold">
            NOVO FIAT 500 ELÉTRICO.
          </Text>
        </Title>
        <Batery className="absolute -bottom-7 w-2/12 lg:w-auto lg:left-24 lg:bottom-1/3" />
      </div>
    </>
  )
}

FirstFoldOrganism.displayName = 'FirstFold^Organism'

export const FirstFold = WithComponent(FirstFoldOrganism)
