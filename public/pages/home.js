import { Grid } from '../atoms/grid'
import { WithComponent } from '../atoms/withComponent'
import { Ratio } from '../atoms/ratio'
import { FirstFold } from '../organisms/firstFold'
import { TweetFold } from '../organisms/tweetFold'
import { FormFold } from '../organisms/formFold'
import useDeviceDetect from '../hooks/useDeviceDetect'

function HomePage({ query, params, ...props }) {
  const [isMobile] = useDeviceDetect()

  return (
    <>
      <section className="relative -top-14" {...props}>
        <Ratio aspect={isMobile ? '375:465' : '1364:675'}>
          <FirstFold />
        </Ratio>
        {isMobile ? (
          <Ratio aspect="375:530">
            <TweetFold />
          </Ratio>
        ) : (
          <TweetFold className="absolute inset-y-0 w-80 right-5" />
        )}
        <Grid
          className="-mt-10 lg:-mt-24 bg-black"
          cols={12}
          span={isMobile ? 10 : 6}
          start={isMobile ? 2 : 4}
        >
          <FormFold />
        </Grid>
      </section>
    </>
  )
}

HomePage.displayName = 'Home^page'

export const Home = WithComponent(HomePage)
