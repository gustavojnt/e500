const NotFound = () => (
  <section data-component="NotFound">
    <h1>404: Not Found</h1>
    <p>It's gone :(</p>
  </section>
);

export default NotFound;
